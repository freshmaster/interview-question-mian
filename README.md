# Interview Question



## Getting started

Please download the files from this repository.  The files include a base theme and a child theme (Freshwater Test) for a new website.  

Complete the following steps and then please push the changes up to the repository using the branch **test-question**. If you have any questions, please contact [David Boutin](mailto:david.boutin@freshwatercreative.ca)

## Add a new page template

Please add a page template to the child theme called "Landing Page." This new landing page template will have a header that's different from the main default template in that it should be kept very simple (should not have the main navigation, top (sub) navigation, social icons, or the main header content image/text.  Do not worry about styling for the template header.  It should be enough to just show the logo.

## Add custom field content

A custom field group called "Landing Page Fields" has been added using Advanced Custom Fields (ACF).  The field group contains the following field:

1. **Page Content** 
   - Field Name: page_content
   - Field Type: Flexible Content
   - Layout: **2 Column Image and Text Row** containing fields: 
      1. *Field Label:* Image, *Name:* image, *Field Type:* Image Array
      2. *Field Label:* Heading, *Name:* heading, *Field Type:* Text
      3. *Field Label:* Content, *Name:* content, *Field Type:* Wysiwyg
   - Layout: **Text/HTML Content** containing fields:
      1. *Field Label:* Heading, *Name:* heading, *Field Type:* Text
      2. *Field Label:* Content, *Name:* content, *Field Type:* Wysiwyg

Please add the field content blocks to the new landing page template that you created previously.  The 2-Column layout can be added as one row with the first column containing the image, the second column containing the heading with content below the heading.  Do not worry about styling the new block just yet.

## Style the new block

Please add the necessary sass files and styles to style the new blocks (do not forget to make sure that Wordpress will add any generated css files to the site pages).  The main template will add the margins already, so you do not need to worry about adding the spacing on the sides for desktop, mobile, or tablet.

The 2-Column layout can be styled as a full-width row of the image and content columns. If more than one of this specific content block types (2 Column Image eand Text Row) are added consecutively, please alternate the image to be on the other column of the row (so that there are never 2 images above each other). 

On mobile and tablet, the 2-column row block's columns should stack on top of each other with the image always above the text. See the below image for an idea of how it should look on the page. You can just use Open Sans font for the font family and the font size and line height can be approximate. 

![Example Image](wp-content/themes/freshwater_test/assets/image/Sample Content.png)

## Submit your changes

Please push your branch up to the git repository and create a merge request to the main branch with David Boutin as the Approver.  If you have any questions, please contact [David Boutin](mailto:david.boutin@freshwatercreative.ca)
