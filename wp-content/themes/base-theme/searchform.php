<form class="search" method="get" action="<?php echo home_url(); ?>" role="search">
	<input class="search-input" type="search" name="s" placeholder="Search">
	<button class="search-submit" type="submit" role="button">Search</button>
</form>