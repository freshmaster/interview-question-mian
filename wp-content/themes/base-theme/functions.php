<?php

/*------------------------------------*\
	External Modules/Files
\*------------------------------------*/

include_once('functionality/utilities.php');

/*------------------------------------*\
	URL Helpers
\*------------------------------------*/

function base_url($path=""){
    return get_bloginfo('url').'/'.$path;
}
function asset_url($path="", $theme="child"){
    $theme = ($theme == 'child') ? get_stylesheet() : get_template();
    return base_url('wp-content/themes/'.$theme.'/assets/'.$path);
}
function ImgUrl($path="", $theme="child"){
    $theme = ($theme == 'child') ? get_stylesheet() : get_template();
    return base_url('wp-content/themes/'.$theme.'/assets/img/'.$path);
}
function JsUrl($path="", $theme="child"){
    $theme = ($theme == 'child') ? get_stylesheet() : get_template();
    return base_url('wp-content/themes/'.$theme.'/assets/js/'.$path);
}
function CssUrl($path="", $theme="child"){
    $theme = ($theme == 'child') ? get_stylesheet() : get_template();
    return base_url('wp-content/themes/'.$theme.'/assets/css/'.$path);
}
function VendorUrl($path="", $theme="child"){
	$theme = ($theme == 'child') ? get_stylesheet() : get_template();
	return base_url('wp-content/themes/'.$theme.'/assets/vendor/'.$path);
}
function NodeUrl($path="", $theme="child"){
    $theme = ($theme == 'child') ? get_stylesheet() : get_template();
    return base_url('wp-content/themes/'.$theme.'/node_modules/'.$path);
}

/*------------------------------------*\
	Theme Support
\*------------------------------------*/

if (function_exists('add_theme_support')){
    // Add Menu Support
    add_theme_support('menus');

    // Add Thumbnail Theme Support
    add_theme_support('post-thumbnails');
    add_image_size('large', 700, '', true); // Large Thumbnail
    add_image_size('medium-large', 450, '', true); // Medium-Large Thumbnail
    add_image_size('medium', 250, '', true); // Medium Thumbnail
    add_image_size('small', 120, '', true); // Small Thumbnail
}

/*------------------------------------*\
	Enqueues
\*------------------------------------*/

function base_enqueues(){    
    // register scripts
    wp_deregister_script('jquery');
    wp_deregister_script('jquery-migrate');
    wp_register_script('jquery', '//code.jquery.com/jquery-2.2.4.min.js', array(), '2.2.4', false);
    // enqueue scripts
	wp_enqueue_script('jquery');
}
add_action( 'wp_enqueue_scripts', 'base_enqueues' );

/*------------------------------------*\
	Remove Excess
\*------------------------------------*/

remove_action('wp_head', 'feed_links_extra', 3); // Display the links to the extra feeds such as category feeds
remove_action('wp_head', 'feed_links', 2); // Display the links to the general feeds: Post and Comment Feed
remove_action('wp_head', 'rsd_link'); // Display the link to the Really Simple Discovery service endpoint, EditURI link
remove_action('wp_head', 'wlwmanifest_link'); // Display the link to the Windows Live Writer manifest file.
remove_action('wp_head', 'wp_generator'); // Display the XHTML generator that is generated on the wp_head hook, WP version
add_filter('xmlrpc_enabled', '__return_false'); //Disable XMLRPC to try and block DDoS

// Remove wp_head() injected Recent Comment styles
function remove_recent_comments_style(){
    global $wp_widget_factory;
    remove_action('wp_head', array(
        $wp_widget_factory->widgets['WP_Widget_Recent_Comments'],
        'recent_comments_style'
    ));
}
add_action('widgets_init', 'remove_recent_comments_style');

// Remove thumbnail width and height dimensions that prevent fluid images in the_thumbnail
function remove_thumbnail_dimensions($html){
    $html = preg_replace('/(width|height)=\"\d*\"\s/', "", $html);
    return $html;
}
add_filter('post_thumbnail_html', 'remove_thumbnail_dimensions', 10); // Remove width and height dynamic attributes to thumbnails
add_filter('image_send_to_editor', 'remove_thumbnail_dimensions', 10); // Remove width and height dynamic attributes to post images

?>
