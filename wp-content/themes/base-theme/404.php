<?php get_header(); ?>

	<main id="error-404" role="main">
        <h2>404 - Page Not Found</h2>
        <div>
            <a href="<?=base_url();?>">Return Home</a>
        </div>
	</main>

<?php get_footer(); ?>
