<?php get_header(); ?>

	<main id="archive" role="main">
		<h2>Archive</h2>
        
        <div id="post-list">
            <?php
                if(have_posts()){
                    while(have_posts()){
                        the_post();
                        get_template_part('partials/listing', 'posts');
                    }
                } else {
                    get_template_part('partials/listing', 'no-results');
                }
            ?>
        </div>
        
	</main>

<?php get_footer(); ?>
