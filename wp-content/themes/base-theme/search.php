<?php get_header(); ?>

	<main id="search" role="main">
    	<h2>Search Results For: <?=get_search_query();?></h2>
    	<?php
            if(have_posts()):
                while(have_posts()):
                    the_post();
        ?>
        <div class="search-result">
            <h3><?php the_title(); ?></h3>
            <p><?php the_excerpt(); ?></p>
        </div>
        <?php
                endwhile;
            endif;
        ?>
	</main>

<?php get_footer(); ?>
