<?php

/*------------------------------------*\
	Page Utilities
\*------------------------------------*/

if(!function_exists('page_title')){
    function page_title($site_name = "", $sep = " | "){
        if($site_name == "") {
            $site_name = get_bloginfo('name');
        }

        $page_title = wp_title('', false);
        if($page_title != '') {
            $page_title .= " | ";
        }

        $page_title .= $site_name;

        return $page_title;
    }
}

if(!function_exists('add_slug_to_body_class')){
    function add_slug_to_body_class($classes){
        global $post;
        if (is_home()) {
            $key = array_search('blog', $classes);
            if ($key > -1) {
                unset($classes[$key]);
            }
        } elseif (is_page()) {
            $classes[] = sanitize_html_class($post->post_name);
        } elseif (is_singular()) {
            $classes[] = sanitize_html_class($post->post_name);
        }

        return $classes;
    }
    add_filter('body_class', 'add_slug_to_body_class');
}

if(!function_exists('get_page_slug')){
    function get_page_slug() {
        global $wp_query;
        $post = get_post();

        // First check for homepage / blog
        $slug = '';
        if ( is_front_page() && is_home() ) {
            // Default homepage
            $slug = $post->post_name;
        } elseif ( is_front_page() ) {
            // static homepage
            $slug = $post->post_name;
        } elseif ( is_home() ) {
            // blog page
            $slug = $post->post_name;
        } else {
            // If it's not homepage/blog, then it's something else (a page, a post)
            if(is_page()) {
                $slug = $post->post_name;
            } else {
                // if we haven't covered all cases, then we end up here
                    $slug = get_post_type() == 'post' ? 'blog' : get_post_type(); // blog is the default post type
                }
        }

        return $slug;
    }
}

/*------------------------------------*\
	Tiny MCE
\*------------------------------------*/

// Modify tinymce defaults
if(!function_exists('base_define_tinymce')){
    function base_define_tinymce($arr){
        $arr['block_formats'] = 'Paragraph=p;Heading 1=h3;Heading 2=h4;Heading 3=h5;';
        $arr[ 'paste_as_text' ] = true;
        return $arr;
    }
    add_filter('tiny_mce_before_init', 'base_define_tinymce');
}

/*------------------------------------*\
	404 Page
\*------------------------------------*/

if(!function_exists('show_404')){
    function show_404()
    {
        global $wp_query;
        $wp_query->is_404 = true;
        $wp_query->is_single = false;
        $wp_query->is_page = false;

        include( get_query_template( '404' ) );
        exit();
    }
}