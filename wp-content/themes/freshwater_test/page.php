<?php get_header(); ?>
    
    <?php if(have_posts()): while(have_posts()): the_post(); ?>

	<main  class="container-fluid" id="basic" role="main">
        <div class="row row-no-padding">
            <div class="default-page-content col-xs-12">
            <?php the_content(); ?>
            
            </div>
        </div>
    </main>
    
    <?php endwhile; endif; ?>

<?php get_footer(); ?>
