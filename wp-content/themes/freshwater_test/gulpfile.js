var gulp = require('gulp');
var cssmin = require('gulp-cssmin');
var concatCss = require('gulp-concat-css');

// Requires the gulp-sass plugin
var sass = require('gulp-sass');

gulp.task('sass', function(){
    return gulp.src('assets/scss/main.scss')
      .pipe(sass()) // Converts Sass to CSS with gulp-sass
      .pipe(concatCss('main.min.css'))
      .pipe(cssmin())
      .pipe(gulp.dest('assets/css'))
  });

gulp.task('default', function() {
    console.log('Run "gulp watch" to compile scss into css');
});

gulp.task('watch', function(){
    gulp.watch([
        'assets/scss/*.scss',
    ], gulp.series('sass'));
// Other watchers
});


// Css task runner here
gulp.task('css_production', function () {
	return gulp.src('assets/scss/main.scss')
      .pipe(sass()) // Converts Sass to CSS with gulp-sass
      .pipe(concatCss('main.min.css'))
      .pipe(cssmin())
      .pipe(gulp.dest('assets/css'))
});

gulp.task('production', function() {
	gulp.start('css_production');
	//gulp.start('js_production');
});

