<!doctype html>  

<!--[if IEMobile 7 ]> <html <?php language_attributes(); ?>class="no-js iem7"> <![endif]-->
<!--[if lt IE 7 ]> <html <?php language_attributes(); ?> class="no-js ie6"> <![endif]-->
<!--[if IE 7 ]>    <html <?php language_attributes(); ?> class="no-js ie7"> <![endif]-->
<!--[if IE 8 ]>    <html <?php language_attributes(); ?> class="no-js ie8"> <![endif]-->
<!--[if (gte IE 9)|(gt IEMobile 7)|!(IEMobile)|!(IE)]><!--><html <?php language_attributes(); ?> class="no-js"><!--<![endif]-->
	
	<head>

		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
		<title><?=page_title();?></title>
		<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
		
		<!-- wordpress head functions -->
		<?php wp_head(); ?>
		<!-- end of wordpress head -->

	</head>
	
	<body id="top"  <?php body_class(); ?>>

        <!-- Header Content  -->
        <?php 
        $header_info = get_field('header');
        $subheading_text = $header_info['subheading_text'];
        $header_text = $header_info['header_text'];
        $new_item = $header_info['new_item'];
        $link = $header_info['header_button'];

        $header_image = $header_info['header_image'];
        $size = 'full'; // (thumbnail, medium, large, full or custom size)
        
        if( $header_image ) {
            $header_image = wp_get_attachment_image( $header_image['id'], $size );
        }
            
    ?>


	<header id="header-container" class="<?php echo $header_image ? 'header-image' : 'no-header-image'; ?>">

        <div class="navbar-container">
            <nav class="top-links">
                <div class="top-links-wrapper">
                <?php wp_nav_menu( array( 'sort_column' => 'menu_order', 'menu_class' => 'nav navbar-nav top-links-nav', 'theme_location' => 'top-menu')); ?><!--end: nav-->
                </div>
            </nav>
            <nav class="navbar navbar-light navbar-static-top navbar-expand-xl" role="navigation">
                <div class="navbar-contents-wrapper">
                    
                    <div class="navbar-header">
                        <?php 
                        $logo_image = get_field('header_logo', 'option');
                        $size = 'full'; // (thumbnail, medium, large, full or custom size)
                        if( $logo_image ) {
                            $logo_url = wp_get_attachment_image_url( $logo_image['id'], $size );
                            $site_title = get_bloginfo( 'name' );
                        }
                        ?>
                        <a class="navbar-brand" href="<?php echo site_url(); ?>">
                        <img width="200" height="42.6" src='<?php echo $logo_url ? $logo_url : "/wp-content/themes/fresgwater_test/assets/image/logo2.png";?>'>
                        <span class="site-title hidden"><?php echo $site_title; ?></span>
                        </a>

                        <button class="navbar-toggler navbar-toggler-right ml-auto" type="button" data-toggle="collapse" data-target=".nav-links" aria-controls="nav-links" aria-expanded="false" aria-label="Toggle navigation">
                            <span class="dashicons dashicons-menu-alt3"></span>
                            <span class="navbar-toggler-close dashicons dashicons-no-alt"></span>
                        </button>
                    </div><!--end: navbar-header-->
                    
                    <div class="nav-links-container ">
                        <div id="nav-links" class="nav-links collapse navbar-collapse pull-right">
                                
                                <?php wp_nav_menu( array( 'sort_column' => 'menu_order', 'menu_class' => 'nav navbar-nav', 'theme_location' => 'main-menu', 'depth' => 2)); ?><!--end: nav-->
                        
                                <div class="section-text mobile-social-links">
                                    <div class="social-links">
                                        <a href="#"><i class="fa fa-facebook-square"></i></a>
                                        <a href="#"><i class="fa fa-instagram"></i></a>
                                        <a href="#"><i class="fa fa-twitter"></i></a>
                                    </div>
                                </div>
                              <!--  <div class="section-text mobile-search-form">
                                    <?php #get_search_form( ); ?>
                                </div> -->

                        </div><!--end: navbar-collapse-->
                    </div>
                </div><!--end: .navbar-contents-wrapper-->
                
            </nav><!--end: navbar-->


        </div>



        <div class="header-content <?php echo $new_item ? 'new-item' : ''; ?> ">
            <div class="header-image">
                <?php echo $header_image; ?>
            </div>
            <div class="header-text">
                <?php the_title( '<h1 class="element-animation hidden">', '</h1>' ); ?>
                <div class="subheading">
                    <h3><?php echo $subheading_text; ?></h3>
                    <h2><?php echo $header_text; ?></h2>
                </div>
                
                <?php 
                    if( $link ): 
                        $link_url = $link['url'];
                        $link_title = $link['title'];
                        $link_target = $link['target'] ? $link['target'] : '_self';
                        ?>
                        <div class="buttons">
                            <a style="border: none" class="btn button" href="<?php echo esc_url( $link_url ); ?>" target="<?php echo esc_attr( $link_target ); ?>"><?php echo esc_html( $link_title ); ?></a>
                        </div>
                    <?php endif; ?>
                
            </div>
        </div>
        
        <div class="sub-nav">
        <?php wp_nav_menu( array( 'sort_column' => 'menu_order', 
                                  'menu_class' => 'nav navbar-nav', 
                                  'sub_menu'      => true,
                                  'direct_parent' => true,
                                  'theme_location' => 'main-menu')); 
         ?>

        </div>  
  
    </header><!--end: container-->
